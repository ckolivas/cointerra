# Asynchronous usb messaging protocol between cgminer and MC    
Rev. 0.9.1, Dec. 30, 2013, Timo Hanke, CoinTerra

## General
### Endpoints

- endpoint 0x01, type bulk in
- endpoint 0x81, type bulk out 

### Data packets

Each data packet is 64 bytes long and contains exactly one message.
The structure is:

```
start sequence | msg type | message body | padding 
```

_start sequence_ is the 2 byte constant 0x5a5a to help with
synchronization.

_msg type_ is 1 byte (there are currently 10 different message types)

_msg body_ is a fixed length serialized data record (0-61 bytes in
length) whose structure depends on _msg type_, the length of each
individual field in the serialization is always a multiple of a byte
(even if its a 1 bit flag)

_padding_ consists of zeros

## Difficulty handling

The only type of difficulties that the MC can handle is two-power
multiples of pool difficulty 1, ie. targets defined by a number of
leading zeros.

Recall that the target of pool difficulty 1 is
```
< 2^(256-32) = 0x00000001000000000000000000000000000000000000000000000000000000000000
```
(resp. `<= 0x00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF`)
and corresponds to 32 leading zeros.

The MC can handle fractional pool difficulties, in fact targets as
high as 0 leading zeros (everything is a match). The
difficulties/targets communicated in the protocol are number of
leading zeros. Valid values are in the range 0-63 and we reserve one
byte for it in the messages.

Each MC can be set to a permanent minimum difficulty (_min diff_). Note
that resetting _min diff_ requires a level 2 reset (new block reset)
which also flushes all queues. The difficulty that comes with the
individual work items (_work diff_) is handled as follows: if 
`work diff < min diff` then the MC does not relay it to the ASICs but
immediately returns it to cgminer as 'work done' with 0
full-nonce-ranges done. If `work diff >= min diff` then the MC does not
guarantee that the work item is actually worked on with _work diff_.
The MC may relay the work with _work diff_ but does not guarantee this.
It only guarantees _min diff_, so it may return more successes to cgminer than cgminer actually wants to
see. cgminer has be to able to handle that and do it's own validation
by re-hashing (which it in most scenarios does anyway). In order to
reduce message traffic cgminer ideally keeps _min diff_ as high as
possible. In particular, cgminer should avoid the initial diff=1 work
from stratum after first connection (even if that results in unclean
code!).

_min diff_ gets adjusted along with a reset message. It is important
that cgminer can handle targets higher than the diff 1 target (ie less
than 32 leading zeros) for testing purposes.

Examples with arbitrary precision computation:
pool diff = 1.0
diff = 0.9999847412109375
target = 0000000100000000000000000000000000000000000000000000000000000000
diffbits = number of leading zeros(target-1) = 32

pool diff = 2.0
diff = 1.999969482421875
target = 0000000080000000000000000000000000000000000000000000000000000000
diffbits = number of leading zeros(target-1) = 33

pool diff = 1024.0
diff = 1023.984375
targer = 0000000000400000000000000000000000000000000000000000000000000000
diffbits = number of leading zeros(target-1) = 42
## Messages
### Message type overview
The numbers in parenthesis is _msg body_'s size in bytes.

For endpoint 0x81 out:

- 00: not used
- 01: reset 
- 02: work 
- 03: set-perf 
- 04: request
- 05: false-match 
- 06: flash-led

For endpoint 0x01 in:

- 00: not used
- 01: req-work
- 02: match 
- 03: work-done 
- 04: status readings 
- 05: status settings
- 06: info
- 07: log
- 08: reset-done
- 09: status debug

### Endianess
All numerical values are stored in little-endian byte order. 
### reset 
Reset MC and/or ASIC in one of several forms.

Bytes:

- 00: level
- 01: diffbits
- 02-03: not used
- 04-07: request id

_level_:

- 00: no reset
- 01: work update
- 02: new block 
- 03: init
- 04-06: same as 00-02 but with diffbits set

_diffbits_: log_2(pool difficulty) + 32 = number of leading zeros that we like the MCU/ASICs to work at
_request id_: will be returned in the corresponding reset done message to help with synchronization, must be non-zero if relied upon
### work
Sends a work unit to the MC.

Bytes:

* 00-01: cgminer tag
* 02-05: MCU tag
* 06-37: midstate
* 38-41: merkle root
* 42-45: timestamp
* 46-49: bits (=compressed blockheader target)
* 50-51: ntime roll limit 
* 52-55: work diff
* 57-59: nonce
### set-perf (unimplemented)
Sets the performance mode (overclocking/voltage) 

Bytes:
### request
Request a message of a certain type from the MC. Currently this is
used only for requesting messages of a status type (type
4,5,9). Besides triggering an immediate response, this can also set
the interval for future responses.

Bytes:

* 00-01: msg type
* 02-03: interval

_type_: type of message requested, currently implemented: 4=status
readings, 5=status settings, 9=status debug
_interval_: interval in which status messages are being sent from now
on, in multiples of 10ms. A value of 0x00 indicates that an immediate
status message is sent, no future ones. A value of 0xFFFF turns off
status messages entirely. MC's internal default values for the
interval after reset are: 0x0032 for type 4 (every 0.5s), 0x0000 for
types 5 and 9 (never).
### false-match
Reports a false match back to the MC (indicating a hardware error). Offsets
are chosen to mirror those in match.

Bytes:

* 00-01: cgminer tag
* 02-05: MCU tag
* 06-51: unused
* 52-55: work diff

_MC tag_ is the MC's internal tag for this work

_diff_ is the difficulty which cgminer got reported from the MC but which cgminer was unable to confirm
### flash-led
Triggers the MC to flash some LEDs

Bytes:

* 00: id of LED or bit-vector of LEDs to set (only constant 0x00 or 0x01 for red and green)
* 01: mode
* 02: argument

_bit vector_ defines which of up to 8 LEDs are to be set (only 1 present)

_mode_ is 0 for off, 1 for on, >= 2 for various MCU internal modes, eg 3 for steady blinking, 4 for blinking with each match

_argument_ is additional argument for MCU internal mode, eg speed of blinking
### req-work
Requests more work

Bytes:

* 00-01: number of work items requested

### match 
Reports a found match 

Bytes:

* 00-01: cgminer tag
* 02-05: MCU tag
* 06-41: not used
* 42-45: timestamp offset
* 46-51: not used
* 52-55: searched diff
* 57-59: successful nonce

_searched diff_ is the diff that was actually compared against when searching for the nonce
### work-done
Reports that work on an item has completed

Bytes:

* 00-01: cgminer tag
* 02-07: not used
* 08-15: nonces covered
* 16-51: not used
* 52-55: searched diff

The MC computes _nonces covered_ excluding the matches that were already
reported earlier in match messages.
_searched diff_ is the diff that was actually compared against when searching for the nonce
### status 0 (readings) 
Reports detailed status of MC and ASICs

Bytes:

00-53 = hardware readings
54-57 = software readings

* 00-15: core temp[8]
* 16-17: ambient temp low
* 18-19: ambient temp avg
* 20-21: ambient temp high
* 22-25: pump tach[2] 
* 26-33: fan tach[4]
* 34-49: core voltage[8]
* 50-51: 3.3V
* 52-53: 12V
* 54-55: inactive works
* 56-57: active works
* 58-60: uptime

_temp_s are in 0.01C
_tach_s are in rpm
_voltage_s are in mV
_inactive works_: number of works queued inside the MC
_active works_: number of work items inside the ASICs
_uptime_ is in 10ms ticks
### status 1 (settings) 
Reports detailed status of MC and ASICs

Bytes:

* 00-07: core performance mode[8]
* 08-11: fan commanded speed[4]
* 12-12: dies active 
* 13-20: pipes enabled count[8] 
*    21: min fan commanded speed

_performance mode_: one of a number of pre-defined modes, not yet implemented, should be 0
_fan commanded speed_: the range 0-255 equates to 0-100% speed
_dies active_: bitvector of active dies
_pipes enabled count_: number of pipes enabled per die
_min commanded fan speed_ : a minimum that overrides the individual commanded fan speeds if they are lower
### debug status
Reports status info relevant for debugging

Bytes:

00-01: underruns
02-17: spi errors[8] 
18-21: uptime
22-29: hashes
30-37: flushed hashes (est.)

_underruns_: total number of queue underrun events inside the MC since startup
_spi errors_: total number of spi errors per die since startup
_uptime_   : is in ms
_hashes_   : is the total hash count since the last init reset
_flushed hashes_: is an estimate of the hashes that were done on work that got flushed by resetting the ASICs
### log
Raw logging message

Bytes:
* 00-60: raw string

### info 
Initial info message providing device information

Bytes:

00-15 = hardware
16-31 = firmware
32-35 = serial number
 
00-01: GSA board hardware revision
02-05: serial no 
06-06: nominal no asics
07-07: nominal no dies
08-09: nominal no cores
10-15: not used 
16-18: _revision_
19-19: three_fan_jumper(board indicator)
20-23:  _date_
24-24: _init diffbits_
25-25: _min diffbits_
26-26: _max diffbits_ 
32-35: _serial number_

_min diffbits_, _max diffbits_ are enforced by the device, it comes up with _init diffbits_.
_board number_ gives information of the position of the board in a device (eg. left/right) and is determined from jumper settings on the board

Firmware may get initially flashed with a _serial number_.
Firmware _revision_ is binary numerical values, e.g. 3.2.11 = 0x03020a.
Firmware _date_ is binary numerical values, e.g. 2013-10-31 = 0x07dd0a0d.
### reset-done
Reports that a reset has been completed and what type of rest it was. When
accompanying a level 2 reset it means there is no more work running or queued
allowing all driver references to be discarded.

Bytes:
* 00: level
* 01: diffbits
* 02-03: not used
* 04-07: request id
* 08-15: nonces covered

_level_ : is the type of reset performed.
_request id_    : is the id number from the corresponding reset request, 0 if the reset is spontaneous (was not requested)
_nonces covered_ : an estimate of how many hashes were performed that
were unaccounted for since work was underway that did not find a nonce and was
aborted without sending a matching work-done.

_diffbits_ is the current diffbits setting inside the MCU after the reset
